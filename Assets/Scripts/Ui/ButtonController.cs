using System;
using Runtime.AdWrappers.Banner;
using Runtime.AdWrappers.Interstitial;
using Runtime.AdWrappers.RewardedVideo;
using Runtime.AdWrappers.RewardInterstitial;
using Zenject;

namespace Ui
{
    public class ButtonController : IInitializable, IDisposable
    {
        private readonly ButtonView _view;
        private readonly IBannerWrapper _bannerWrapper;
        private readonly IInterstitialWrapper _interstitialWrapper;
        private readonly IRewardedVideoWrapper _rewardedVideoWrapper;
        private readonly IRewardedInterstitialWrapper _rewardedInterstitialWrapper;

        public ButtonController(
            ButtonView view,
            IBannerWrapper bannerWrapper,
            IInterstitialWrapper interstitialWrapper,
            IRewardedVideoWrapper rewardedVideoWrapper,
            IRewardedInterstitialWrapper rewardedInterstitialWrapper
        )
        {
            _view = view;
            _bannerWrapper = bannerWrapper;
            _interstitialWrapper = interstitialWrapper;
            _rewardedVideoWrapper = rewardedVideoWrapper;
            _rewardedInterstitialWrapper = rewardedInterstitialWrapper;
        }

        public void Initialize()
        {
            _view.ShowBanner.onClick.AddListener(ShowBanner);
            _view.HideBanner.onClick.AddListener(HideBanner);
            _view.DestroyBanner.onClick.AddListener(DestroyBanner);
            _view.ShowInterstitial.onClick
                .AddListener(ShowInterstitial);
            _view.ShowRewardedVideo.onClick.AddListener(ShowRewardedVideo);
            _view.ShowRewardedInterstitial.onClick.AddListener(ShowRewardedInterstitial);
        }

        public void Dispose()
        {
            _view.ShowBanner.onClick.RemoveListener(ShowBanner);
            _view.HideBanner.onClick.RemoveListener(HideBanner);
            _view.DestroyBanner.onClick.RemoveListener(DestroyBanner);
            _view.ShowInterstitial.onClick
                .RemoveListener(ShowInterstitial);
            _view.ShowRewardedVideo.onClick.RemoveListener(ShowRewardedVideo);
            _view.ShowRewardedInterstitial.onClick.RemoveListener(ShowRewardedInterstitial);

        }

        private void ShowBanner() => _bannerWrapper.ShowBanner();
        
        private void HideBanner() => _bannerWrapper.HideBanner();

        private void DestroyBanner() => _bannerWrapper.DestroyBanner();

        private void ShowInterstitial() => _interstitialWrapper.Show(OnComplete);

        private void ShowRewardedVideo() => _rewardedVideoWrapper.Show(OnComplete);

        private void ShowRewardedInterstitial() =>
            _rewardedInterstitialWrapper.Show(OnComplete);

        private static void OnComplete()
        {
        }
    }
}