using UnityEngine;
using UnityEngine.UI;

namespace Ui
{
    public class ButtonView : MonoBehaviour
    {
        [SerializeField] private Button showBanner;
        [SerializeField] private Button hideBanner;
        [SerializeField] private Button destroyBanner;
        [SerializeField] private Button showInterstitial;
        [SerializeField] private Button showRewardedVideo;
        [SerializeField] private Button showRewardedInterstitial;
        [SerializeField] private DebugConsole debugConsole;

        public Button ShowBanner => showBanner;

        public Button HideBanner => hideBanner;

        public Button DestroyBanner => destroyBanner;

        public Button ShowInterstitial => showInterstitial;

        public Button ShowRewardedVideo => showRewardedVideo;

        public Button ShowRewardedInterstitial => showRewardedInterstitial;

        public DebugConsole DebugConsole => debugConsole;
    }
}