using TMPro;
using UnityEngine;

namespace Ui
{
    public class DebugConsole : MonoBehaviour   
    {
        [SerializeField] private TMP_Text debugText;

        public void SetText(string text) => debugText.text = text;
    }
}