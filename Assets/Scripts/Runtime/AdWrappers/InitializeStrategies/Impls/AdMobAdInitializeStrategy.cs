using System;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Runtime.AdWrappers.InitializeStrategies.Impls
{
    public class AdMobAdInitializeStrategy : IAdInitializeStrategy
    {
        public event Action OnInitialized;

        public void Initialize()
        {
            var requestConfiguration = new RequestConfiguration
            {
                TagForChildDirectedTreatment = TagForChildDirectedTreatment.Unspecified
            };

            MobileAds.SetRequestConfiguration(requestConfiguration);
            MobileAds.Initialize(OnInitComplete);
        }
        
        private void OnInitComplete(InitializationStatus status)
        {
            Debug.Log("AdMob initialization complete.");
            OnInitialized?.Invoke();
        }
    }
}