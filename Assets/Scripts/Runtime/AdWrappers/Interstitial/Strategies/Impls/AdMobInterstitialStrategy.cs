using System;
using GoogleMobileAds.Api;
using Runtime.Settings;
using UnityEngine;
using Zenject;

namespace Runtime.AdWrappers.Interstitial.Strategies.Impls
{
    public class AdMobInterstitialStrategy : IAdMobInterstitialStrategy, ITickable, IInitializable
    {
        private readonly IAdSettingsDatabase _adSettingsDatabase;

        private InterstitialAd _interstitialAd;
        private float _elapsedTime;
        private int _currentLoadingAttempt;

        public event Action OnAdLoaded;
        
        public bool IsLoaded => _interstitialAd != null && _interstitialAd.CanShowAd();

        public AdMobInterstitialStrategy(
            IAdSettingsDatabase adSettingsDatabase
        )
        {
            _adSettingsDatabase = adSettingsDatabase;
        }

        public void Initialize() => Preload();

        public void Tick()
        {
            if (IsLoaded || _elapsedTime >= _adSettingsDatabase.PreloadingInterval)
                return;

            _elapsedTime += Time.deltaTime;

            if (_elapsedTime >= _adSettingsDatabase.PreloadingInterval)
                Preload();
        }

        public void Preload()
        {
            Debug.Log("Start interstitial loading");
            var interstitialId = _adSettingsDatabase.GetInterstitialId();
            _interstitialAd?.Destroy();
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-interstitial");

            void OnInterstitialLoad(InterstitialAd ad, AdError error)
            {
                if (error != null)
                {
                    Debug.Log($"Interstitial error: {error.GetMessage()}");
                    _currentLoadingAttempt++;

                    if (_currentLoadingAttempt >= _adSettingsDatabase.LoadingAttempts)
                    {
                        _elapsedTime = 0;
                        return;
                    }

                    Preload();
                    return;
                }

                Debug.Log("Interstitial was loaded");
                OnAdLoaded?.Invoke();
                _interstitialAd = ad;
                _currentLoadingAttempt = 0;
            }

            InterstitialAd.Load(interstitialId, adRequest, OnInterstitialLoad);
        }

        public void Show(Action onComplete, Action onError = null)
        {
            void OnInterstitialShown()
            {
                Debug.Log("Interstitial was shown");
                _interstitialAd.OnAdFullScreenContentClosed -= OnInterstitialShown;
                onComplete();
                Preload();
            }

            _interstitialAd.OnAdFullScreenContentClosed += OnInterstitialShown;
            _interstitialAd.Show();
        }
    }
}