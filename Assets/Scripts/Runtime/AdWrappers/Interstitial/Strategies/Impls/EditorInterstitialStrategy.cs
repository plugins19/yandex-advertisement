using System;
using UnityEngine;

namespace Runtime.AdWrappers.Interstitial.Strategies.Impls
{
    public class EditorInterstitialStrategy : IEditorInterstitialStrategy
    {
        public event Action OnAdLoaded;
        public bool IsLoaded => true;

        public void Preload()
        {
            Debug.Log("Interstitial loaded");
            OnAdLoaded?.Invoke();
        }

        public void Show(Action onComplete, Action onError = null)
        {
            Debug.Log("Interstitial was shown");
            onComplete();
        }
    }
}