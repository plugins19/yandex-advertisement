using System;
using System.Runtime.InteropServices;
using UnityEngine;
using Zenject;

namespace Runtime.AdWrappers.Interstitial.Strategies.Impls
{
    public class YandexGameInterstitialStrategy : IYandexGameInterstitialStrategy, ITickable
    {
        private const float INTERSTITIAL_RELOADING = 60;
        
        private Action _onComplete;
        private Action _onError;
        private float _elapsedTime;

        public event Action OnAdLoaded;
        
        public bool IsLoaded => _elapsedTime >= INTERSTITIAL_RELOADING;

        public void Tick()
        {
            if(IsLoaded)
                return;

            _elapsedTime += Time.deltaTime;
        }

        public void Preload() => OnAdLoaded?.Invoke();

        public void Show(Action onComplete, Action onError = null)
        {
            _onComplete = onComplete;
            _onError = onError;
            ShowYandexInterstitialInternal();
        }

        public void ExecuteSuccess()
        {
            _onComplete();
            Debug.Log("Interstitial success.");
            _elapsedTime = 0;
        }

        public void ExecuteError() => ExecuteError("error");

        public void ExecuteOffline() => ExecuteError("offline");

        public void ExecuteNonShown() => ExecuteError("non shown");

        private void ExecuteError(string logArgument)
        {
            _onError?.Invoke();
            Debug.Log($"Interstitial {logArgument}.");
        }
        
        [DllImport("__Internal")]
        private static extern void ShowYandexInterstitialInternal();
    }
}