using System;

namespace Runtime.AdWrappers.Interstitial.Strategies
{
    public interface IInterstitialStrategy
    {
        event Action OnAdLoaded;
        
        bool IsLoaded { get; }
        
        void Preload();

        void Show(Action onComplete, Action onError = null);
    }
}