namespace Runtime.AdWrappers.Interstitial
{
    public enum EInterstitialResult : ushort
    {
        None = 0,
        Success = 1,
        Error = 2,
        Offline = 3,
        NoneShown = 4,
    }
}