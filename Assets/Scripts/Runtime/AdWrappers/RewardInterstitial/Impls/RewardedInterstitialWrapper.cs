using System;
using Runtime.AdWrappers.RewardInterstitial.Strategies;

namespace Runtime.AdWrappers.RewardInterstitial.Impls
{
	public class RewardedInterstitialWrapper : IRewardedInterstitialWrapper, IDisposable
	{
		private IRewardedInterstitialStrategy _rewardedInterstitialStrategy;

		public event Action OnAdLoaded;

		public bool IsLoaded => _rewardedInterstitialStrategy.IsLoaded;

		public RewardedInterstitialWrapper(
			IRewardedInterstitialStrategy rewardedInterstitialStrategy
		)
		{
			_rewardedInterstitialStrategy = rewardedInterstitialStrategy;
			_rewardedInterstitialStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		public void Dispose() => _rewardedInterstitialStrategy.OnAdLoaded -= OnAdLoadedEvent;

		public void Preload() => _rewardedInterstitialStrategy.Preload();

		public void Show(Action onComplete, Action onClose = null, Action onError = null)
			=> _rewardedInterstitialStrategy.Show(onComplete, onClose, onError);

		public void ChangeStrategy(IRewardedInterstitialStrategy strategy)
		{
			_rewardedInterstitialStrategy.OnAdLoaded -= OnAdLoadedEvent;
			_rewardedInterstitialStrategy = strategy;
			_rewardedInterstitialStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		private void OnAdLoadedEvent() => OnAdLoaded?.Invoke();
	}
}