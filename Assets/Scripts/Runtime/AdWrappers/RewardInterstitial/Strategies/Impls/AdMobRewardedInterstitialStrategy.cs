using System;
using GoogleMobileAds.Api;
using Runtime.Settings;
using UnityEngine;
using Zenject;

namespace Runtime.AdWrappers.RewardInterstitial.Strategies.Impls
{
	public class AdMobRewardedInterstitialStrategy : IAdMobRewardedInterstitialStrategy, IInitializable, ITickable
	{
		private readonly IAdSettingsDatabase _adSettingsDatabase;

		private RewardedInterstitialAd _rewardedInterstitialAd;
		private float _elapsedTime;
		private int _currentLoadingAttempt;

		public event Action OnAdLoaded;

		public bool IsLoaded => _rewardedInterstitialAd != null && _rewardedInterstitialAd.CanShowAd();

		public AdMobRewardedInterstitialStrategy(
			IAdSettingsDatabase adSettingsDatabase
		)
		{
			_adSettingsDatabase = adSettingsDatabase;
		}

		public void Initialize() => Preload();

		public void Tick()
		{
			if (IsLoaded || _elapsedTime >= _adSettingsDatabase.PreloadingInterval)
				return;

			_elapsedTime += Time.deltaTime;

			if (_elapsedTime >= _adSettingsDatabase.PreloadingInterval)
				Preload();
		}

		public void Preload()
		{
			Debug.Log("Start rewarded interstitial loading");
			var rewardedInterstitialId = _adSettingsDatabase.GetRewardedInterstitialId();
			_rewardedInterstitialAd?.Destroy();
			var adRequest = new AdRequest();
			adRequest.Keywords.Add("unity-admob-rewarded-interstitial");

			void OnRewardedInterstitialLoad(RewardedInterstitialAd ad, AdError error)
			{
				if (error != null)
				{
					Debug.Log($"Rewarded video error: {error.GetMessage()}");
					_currentLoadingAttempt++;

					if (_currentLoadingAttempt >= _adSettingsDatabase.LoadingAttempts)
					{
						_elapsedTime = 0;
						return;
					}

					Preload();
					return;
				}

				Debug.Log("Rewarded interstitial was loaded");
				OnAdLoaded?.Invoke();
				_rewardedInterstitialAd = ad;
				_currentLoadingAttempt = 0;
			}

			RewardedInterstitialAd.Load(rewardedInterstitialId, adRequest, OnRewardedInterstitialLoad);
		}

		public void Show(Action onComplete, Action onClose = null, Action onError = null)
		{
			_rewardedInterstitialAd.OnAdFullScreenContentClosed += OnRewardedInterstitialClosed;
			_rewardedInterstitialAd.OnAdFullScreenContentFailed += OnRewardedInterstitialContentFailed;
			_rewardedInterstitialAd.Show(OnReward);

			void OnReward(Reward reward)
			{
				_rewardedInterstitialAd.OnAdFullScreenContentFailed -= OnRewardedInterstitialContentFailed;
				Debug.Log("Reward interstitial was rewarded");
				onComplete();
			}

			void OnRewardedInterstitialContentFailed(AdError error)
			{
				_rewardedInterstitialAd.OnAdFullScreenContentClosed -= OnRewardedInterstitialClosed;
				_rewardedInterstitialAd.OnAdFullScreenContentFailed -= OnRewardedInterstitialContentFailed;
				Debug.Log($"Rewarded interstitial error: {error.GetMessage()}");
				onError?.Invoke();
			}

			void OnRewardedInterstitialClosed()
			{
				_rewardedInterstitialAd.OnAdFullScreenContentClosed -= OnRewardedInterstitialClosed;
				_rewardedInterstitialAd.OnAdFullScreenContentFailed -= OnRewardedInterstitialContentFailed;
				Debug.Log("Rewarded interstitial was closed");
				onClose?.Invoke();
				Preload();
			}
		}
	}
}