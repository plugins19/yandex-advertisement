using System;
using UnityEngine;

namespace Runtime.AdWrappers.RewardInterstitial.Strategies.Impls
{
    public class EmptyRewardedInterstitialStrategy : IEmptyRewardedInterstitialStrategy
    {
        public event Action OnAdLoaded;
        
        public bool IsLoaded => true;

        public void Preload()
        {
            Debug.Log("Rewarded interstitial was loaded");
            OnAdLoaded?.Invoke();
        }

        public void Show(Action onComplete, Action onClose = null, Action onError = null)
        {
            Debug.Log("Rewarded interstitial was shown");
            onComplete();
        }
    }
}