using System;

namespace Runtime.AdWrappers.RewardInterstitial.Strategies
{
    public interface IRewardedInterstitialStrategy
    {
        event Action OnAdLoaded; 
            
        bool IsLoaded { get; }

        void Preload();
        
        void Show(Action onComplete, Action onClose = null, Action onError = null);
    }
}