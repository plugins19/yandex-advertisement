using System;
using Runtime.AdWrappers.RewardedVideo.Strategies;

namespace Runtime.AdWrappers.RewardedVideo.Impls
{
	public class RewardedVideoWrapper : IRewardedVideoWrapper, IDisposable
	{
		private IRewardedVideoStrategy _rewardedVideoStrategy;

		public event Action OnAdLoaded;

		public bool IsLoaded => _rewardedVideoStrategy.IsLoaded;

		public RewardedVideoWrapper(
			IRewardedVideoStrategy rewardedVideoStrategy
		)
		{
			_rewardedVideoStrategy = rewardedVideoStrategy;
			_rewardedVideoStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		public void Dispose() => _rewardedVideoStrategy.OnAdLoaded -= OnAdLoadedEvent;

		public void Preload() => _rewardedVideoStrategy.Preload();

		public void Show(Action onComplete, Action onClose = null, Action onError = null)
			=> _rewardedVideoStrategy.Show(onComplete, onClose, onError);

		public void ChangeStrategy(IRewardedVideoStrategy strategy)
		{
			_rewardedVideoStrategy.OnAdLoaded -= OnAdLoadedEvent;
			_rewardedVideoStrategy = strategy;
			_rewardedVideoStrategy.OnAdLoaded += OnAdLoadedEvent;
		}

		private void OnAdLoadedEvent() => OnAdLoaded?.Invoke();
	}
}