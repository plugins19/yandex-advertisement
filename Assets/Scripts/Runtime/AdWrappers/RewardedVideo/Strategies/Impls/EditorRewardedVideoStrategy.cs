using System;
using UnityEngine;

namespace Runtime.AdWrappers.RewardedVideo.Strategies.Impls
{
    public class EditorRewardedVideoStrategy : IEditorRewardedVideoStrategy
    {
        public event Action OnAdLoaded;
        
        public bool IsLoaded => true;

        public void Preload()
        {
            Debug.Log("Rewarded video loaded");
            OnAdLoaded?.Invoke();
        }

        public void Show(Action onComplete, Action onClose = null, Action onError = null)
        {
            Debug.Log("Rewarded video was shown");
            onComplete();
        }
    }
}