using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Runtime.AdWrappers.RewardedVideo.Strategies.Impls
{
    public class YandexGameRewardedVideoStrategy : IYandexGameRewardedVideoStrategy
    {
        private Action _onComplete;
        private Action _onError;
        private Action _onClose;

        public event Action OnAdLoaded;
        
        public bool IsLoaded => true;

        public void Preload() => OnAdLoaded?.Invoke();

        public void Show(Action onComplete, Action onClose = null, Action onError = null)
        {
            _onComplete = onComplete;
            _onError = onError;
            _onClose = onClose;
            ShowYandexRewardedVideoInternal();
        }

        public void ExecuteRewarded()
        {
            _onComplete();
            Debug.Log("Rewarded video success.");
        }

        public void ExecuteError()
        {
            _onError?.Invoke();
            Debug.Log("Rewarded video error.");
        }

        public void ExecuteClosed()
        {
            _onClose?.Invoke();
            Debug.Log("Rewarded video was skipped.");
        }
        
        [DllImport("__Internal")]
        private static extern void ShowYandexRewardedVideoInternal();
    }
}