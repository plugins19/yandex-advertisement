using System;
using GoogleMobileAds.Api;
using Runtime.Settings;
using UnityEngine;
using Zenject;

namespace Runtime.AdWrappers.RewardedVideo.Strategies.Impls
{
    public class AdMobRewardedVideoStrategy : IAdMobRewardedVideoStrategy, IInitializable, ITickable
    {
        private readonly IAdSettingsDatabase _adSettingsDatabase;

        private RewardedAd _rewardedAd;
        private float _elapsedTime;
        private int _currentLoadingAttempt;

        public event Action OnAdLoaded;
        
        public bool IsLoaded => _rewardedAd != null && _rewardedAd.CanShowAd();

        public AdMobRewardedVideoStrategy(
            IAdSettingsDatabase adSettingsDatabase
        )
        {
            _adSettingsDatabase = adSettingsDatabase;
        }

        public void Initialize() => Preload();

        public void Tick()
        {
            if (IsLoaded || _elapsedTime >= _adSettingsDatabase.PreloadingInterval)
                return;

            _elapsedTime += Time.deltaTime;

            if (_elapsedTime >= _adSettingsDatabase.PreloadingInterval)
                Preload();
        }

        public void Preload()
        {
            Debug.Log("Start rewarded video loading");
            var rewardedVideoID = _adSettingsDatabase.GetRewardedVideoID();
            _rewardedAd?.Destroy();
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-rewarded-video");

            void OnRewardedLoad(RewardedAd ad, AdError error)
            {
                if (error != null)
                {
                    Debug.Log($"Rewarded video error: {error.GetMessage()}");
                    _currentLoadingAttempt++;

                    if (_currentLoadingAttempt >= _adSettingsDatabase.LoadingAttempts)
                    {
                        _elapsedTime = 0;
                        return;
                    }

                    Preload();
                    return;
                }

                Debug.Log("Rewarded video was loaded");
                OnAdLoaded?.Invoke();
                _rewardedAd = ad;
                _currentLoadingAttempt = 0;
            }

            RewardedAd.Load(rewardedVideoID, adRequest, OnRewardedLoad);
        }

        public void Show(Action onComplete, Action onClose = null, Action onError = null)
        {
            _rewardedAd.OnAdFullScreenContentClosed += OnRewardedVideoClosed;
            _rewardedAd.OnAdFullScreenContentFailed += OnRewardedVideoContentFailed;
            _rewardedAd.Show(OnReward);

            void OnReward(Reward reward)
            {
                Debug.Log("Rewarded video was rewarded");
                onComplete();
            }

            void OnRewardedVideoContentFailed(AdError error)
            {
                _rewardedAd.OnAdFullScreenContentFailed -= OnRewardedVideoContentFailed;
                _rewardedAd.OnAdFullScreenContentClosed -= OnRewardedVideoClosed;
                Debug.Log($"Rewarded video was loaded with error: {error.GetMessage()}");
                onError?.Invoke();
            }

            void OnRewardedVideoClosed()
            {
                _rewardedAd.OnAdFullScreenContentClosed -= OnRewardedVideoClosed;
                _rewardedAd.OnAdFullScreenContentFailed -= OnRewardedVideoContentFailed;
                Debug.Log("Rewarded video was closed");
                onClose?.Invoke();
                Preload();
            }
        }
    }
}