using System;
using Runtime.AdWrappers.RewardedVideo.Strategies;

namespace Runtime.AdWrappers.RewardedVideo
{
	public interface IRewardedVideoWrapper
	{
		event Action OnAdLoaded;
        
		bool IsLoaded { get; }

		void Preload();
        
		void Show(Action onComplete, Action onClose = null, Action onError = null);

		void ChangeStrategy(IRewardedVideoStrategy strategy);
	}
}