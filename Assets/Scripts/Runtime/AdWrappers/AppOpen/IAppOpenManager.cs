using System;

namespace Runtime.AdWrappers.AppOpen
{
    public interface IAppOpenManager
    {
        event Action<bool> OnShownSucceed;
        
        void PreloadAndShowAppOpen();
    }
}