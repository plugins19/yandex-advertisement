using System;
using Runtime.AdWrappers.InitializeStrategies;

namespace Runtime.AdWrappers.Main.Impls
{
    public class SimpleAdInitializeManager : IAdInitializeManager
    {
        private readonly IAdInitializeStrategy _adInitializeStrategy;

        public event Action<bool> AppOpenWasShown;
        
        public SimpleAdInitializeManager(
            IAdInitializeStrategy adInitializeStrategy
        )
        {
            _adInitializeStrategy = adInitializeStrategy;
        }

        public void Initialize()
        {
            _adInitializeStrategy.Initialize();
            AppOpenWasShown?.Invoke(true);
        }
    }
}