using UnityEngine;

namespace Runtime.AdWrappers.Banner.Strategies.Impls
{
    public abstract class ASimpleBannerStrategy : IBannerStrategy
    {
        public bool IsBannerActive { get; private set; }

        public float Width
        {
            get
            {
                Debug.LogWarning(
                    $"{nameof(ASimpleBannerStrategy)} You tried to get banner width, but Ad api can't do it."
                );
                return 0;
            }
        }

        public float Height
        {
            get
            {
                Debug.LogWarning(
                    $"{nameof(ASimpleBannerStrategy)} You tried to get banner height, but Ad api can't do it."
                );
                return 0;
            }
        }

        public void ShowBanner(EBannerPosition bannerPosition, EBannerType type, int width = 0, int height = 0)
        {
            if (IsBannerActive)
            {
                Debug.LogError($"{nameof(ASimpleBannerStrategy)} You're trying to show banner, but banner is showing.");
                return;
            }

            IsBannerActive = true;
            ShowBannerInternal();
        }

        public void HideBanner()
        {
            IsBannerActive = false;
            HideBannerInternal();
        }

        public void SetPosition(EBannerPosition position) =>
            Debug.LogWarning(
                $"{nameof(ASimpleBannerStrategy)} You tried to change banner position, but Ad api can't do it."
            );

        public void SetPosition(int x, int y) =>
            Debug.LogWarning(
                $"{nameof(ASimpleBannerStrategy)} You tried to change banner position, but Ad api can't do it."
            );

        public void DestroyBanner() => HideBanner();

        protected abstract void ShowBannerInternal();

        protected abstract void HideBannerInternal();
    }
}