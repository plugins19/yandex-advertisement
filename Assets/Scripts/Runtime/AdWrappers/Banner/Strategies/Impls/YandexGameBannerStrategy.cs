using System.Runtime.InteropServices;

namespace Runtime.AdWrappers.Banner.Strategies.Impls
{
    public class YandexGameBannerStrategy : ASimpleBannerStrategy, IYandexGameBannerStrategy
    {
        protected override void ShowBannerInternal() => ShowBannerInternalFromSDK();

        protected override void HideBannerInternal() => HideBannerInternalFromSDK();

        [DllImport("__Internal")]
        private static extern void ShowBannerInternalFromSDK();

        [DllImport("__Internal")]
        private static extern void HideBannerInternalFromSDK();
    }
}