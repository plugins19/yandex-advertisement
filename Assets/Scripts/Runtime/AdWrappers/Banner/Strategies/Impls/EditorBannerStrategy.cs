using UnityEngine;

namespace Runtime.AdWrappers.Banner.Strategies.Impls
{
    public class EditorBannerStrategy : ASimpleBannerStrategy, IEditorBannerStrategy
    {
        protected override void ShowBannerInternal() => Debug.Log("Banner shown");

        protected override void HideBannerInternal() => Debug.Log("Banner hidden");
    }
}