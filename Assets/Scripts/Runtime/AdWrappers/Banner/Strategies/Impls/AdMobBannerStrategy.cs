using System;
using GoogleMobileAds.Api;
using Runtime.Settings;
using UnityEngine;

namespace Runtime.AdWrappers.Banner.Strategies.Impls
{
    public class AdMobBannerStrategy : IAdMobBannerStrategy
    {
        private readonly IAdSettingsDatabase _adSettingsDatabase;
        
        private BannerView _bannerView;

        public bool IsBannerActive { get; private set; }

        public float Width => _bannerView?.GetWidthInPixels() ?? 0;

        public float Height => _bannerView?.GetHeightInPixels() ?? 0;

        public AdMobBannerStrategy(
            IAdSettingsDatabase adSettingsDatabase
        )
        {
            _adSettingsDatabase = adSettingsDatabase;
        }

        public void ShowBanner(
            EBannerPosition bannerPosition,
            EBannerType type,
            int width = 0,
            int height = 0
        )
        {
            Debug.Log($"{nameof(AdMobBannerStrategy)} Banner request");
            var bannerId = _adSettingsDatabase.GetBannerId();
            _bannerView?.Destroy();
            var position = TransformBannerPosition(bannerPosition);
            var adSize = GetSizeByType(type, width, height);
            _bannerView = new BannerView(bannerId, adSize, position);
            _bannerView.OnBannerAdLoaded += OnBannerLoaded;
            _bannerView.OnBannerAdLoadFailed += OnBannerFailed;
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-banner");
            _bannerView.LoadAd(adRequest);
        }

        public void HideBanner()
        {
            if (_bannerView == null)
            {
                Debug.LogError($"{nameof(AdMobBannerStrategy)} You're trying to hide banner, but banner is destroyed.");
                return;
            }

            if (!IsBannerActive)
            {
                Debug.LogError($"{nameof(AdMobBannerStrategy)} You're trying to hide banner, but banner is hiding.");
                return;
            }

            IsBannerActive = false;
            _bannerView?.Hide();
        }

        public void SetPosition(EBannerPosition position)
        {
            if (_bannerView == null)
                return;

            var adPosition = TransformBannerPosition(position);
            _bannerView.SetPosition(adPosition);
        }

        public void SetPosition(int x, int y) => _bannerView?.SetPosition(x, y);

        public void DestroyBanner()
        {
            _bannerView?.Destroy();
            _bannerView = null;
        }

        private static AdPosition TransformBannerPosition(EBannerPosition position)
        {
            var adPosition = position switch
            {
                EBannerPosition.Top => AdPosition.Top,
                EBannerPosition.Bottom => AdPosition.Bottom,
                EBannerPosition.TopLeft => AdPosition.TopLeft,
                EBannerPosition.TopRight => AdPosition.TopRight,
                EBannerPosition.BottomLeft => AdPosition.BottomLeft,
                EBannerPosition.BottomRight => AdPosition.BottomRight,
                EBannerPosition.Center => AdPosition.Center,
                _ => throw new ArgumentOutOfRangeException(nameof(position), position, null)
            };
            return adPosition;
        }

        private static AdSize GetSizeByType(EBannerType type, int width = 0, int height = 0) =>
            type switch
            {
                EBannerType.Banner => AdSize.Banner,
                EBannerType.MediumRectangle => AdSize.MediumRectangle,
                EBannerType.IABBanner => AdSize.IABBanner,
                EBannerType.Leaderboard => AdSize.Leaderboard,
                EBannerType.SmartBanner => GetAnchoredAdaptiveSize(),
                EBannerType.Custom => new AdSize(width, height),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };

        private void OnBannerLoaded()
        {
            Debug.Log($"{nameof(AdMobBannerStrategy)} Banner loaded");
            IsBannerActive = true;
        }

        private static void OnBannerFailed(LoadAdError error) => Debug.Log(
            $"{nameof(AdMobBannerStrategy)} Banner loading failed. Reason: {error.GetMessage()}"
        );

        private static AdSize GetAnchoredAdaptiveSize()
        {
            var width = Screen.width;
            return AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(width);
        }
    }
}