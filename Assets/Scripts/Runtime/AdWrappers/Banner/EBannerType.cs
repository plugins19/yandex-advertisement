namespace Runtime.AdWrappers.Banner
{
    public enum EBannerType : byte
    {
        Banner,
        MediumRectangle,
        IABBanner,
        Leaderboard,
        SmartBanner,
        Custom,
    }
}