namespace Runtime.Settings
{
    public enum EAdPlatformType : byte
    {
        Editor,
        AndroidTest,
        Android,
        IOSTest,
        IOS,
    }
}