using System;
using UnityEngine;

namespace Runtime.Settings.Impls
{
    [CreateAssetMenu(
        menuName = "Databases/AdSettings/" + nameof(AdMobSettingsDatabase),
        fileName = nameof(AdMobSettingsDatabase), order = 0
    )]
    public class AdMobSettingsDatabase : ScriptableObject, IAdSettingsDatabase
    {
        private const string EDITOR_ID = "unused";
        
        [Header("Ad platform")]
        [SerializeField] private EAdPlatformType adPlatformType;
        
        [Space]
        [Header("Loading parameters")]
        [SerializeField] private float preloadingInterval = 30f;
        [SerializeField] private int loadingAttempts = 5;
        
        [Space]
        [Header("Banner")] 
        [SerializeField] private string bannerAndroidTest = "ca-app-pub-3940256099942544/6300978111";
        [SerializeField] private string bannerAndroid;
        [SerializeField] private string bannerIOSTest = "ca-app-pub-3940256099942544/2934735716";
        [SerializeField] private string bannerIOS;
        
        [Space]
        [Header("App open")]
        [SerializeField] private string appOpenAndroidTest = "ca-app-pub-3940256099942544/3419835294";
        [SerializeField] private string appOpenAndroid;
        [SerializeField] private string appOpenIOSTest = "ca-app-pub-3940256099942544/5662855259";
        [SerializeField] private string appOpenIOS;
        
        [Space]
        [Header("Interstitial")]
        [SerializeField] private string interstitialAndroidTest = "ca-app-pub-3940256099942544/1033173712";
        [SerializeField] private string interstitialAndroid;
        [SerializeField] private string interstitialIOSTest = "ca-app-pub-3940256099942544/4411468910";
        [SerializeField] private string interstitialIOS;
        
        [Space]
        [Header("Rewarded video")]
        [SerializeField] private string rewardedVideoAndroidTest = "ca-app-pub-3940256099942544/5224354917";
        [SerializeField] private string rewardedVideoAndroid;
        [SerializeField] private string rewardedVideoIOSTest = "ca-app-pub-3940256099942544/1712485313";
        [SerializeField] private string rewardedVideoIOS;
        
        [Space]
        [Header("RewardedInterstitial")]
        [SerializeField] private string rewardedInterstitialAndroidTest = "ca-app-pub-3940256099942544/5354046379";
        [SerializeField] private string rewardedInterstitialAndroid;
        [SerializeField] private string rewardedInterstitialIOSTest = "ca-app-pub-3940256099942544/6978759866";
        [SerializeField] private string rewardedInterstitialIOS;

        public float PreloadingInterval => preloadingInterval;
        
        public int LoadingAttempts => loadingAttempts;

        public string GetBannerId()
        {
            return adPlatformType switch
            {
                EAdPlatformType.Editor => EDITOR_ID,
                EAdPlatformType.AndroidTest => bannerAndroidTest,
                EAdPlatformType.Android => bannerAndroid,
                EAdPlatformType.IOSTest => bannerIOSTest,
                EAdPlatformType.IOS => bannerIOS,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string GetAppOpenId()
        {
            return adPlatformType switch
            {
                EAdPlatformType.Editor => EDITOR_ID,
                EAdPlatformType.AndroidTest => appOpenAndroidTest,
                EAdPlatformType.Android => appOpenAndroid,
                EAdPlatformType.IOSTest => appOpenIOSTest,
                EAdPlatformType.IOS => appOpenIOS,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string GetInterstitialId()
        {
            return adPlatformType switch
            {
                EAdPlatformType.Editor => EDITOR_ID,
                EAdPlatformType.AndroidTest => interstitialAndroidTest,
                EAdPlatformType.Android => interstitialAndroid,
                EAdPlatformType.IOSTest => interstitialIOSTest,
                EAdPlatformType.IOS => interstitialIOS,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string GetRewardedVideoID()
        {
            return adPlatformType switch
            {
                EAdPlatformType.Editor => EDITOR_ID,
                EAdPlatformType.AndroidTest => rewardedVideoAndroidTest,
                EAdPlatformType.Android => rewardedVideoAndroid,
                EAdPlatformType.IOSTest => rewardedVideoIOSTest,
                EAdPlatformType.IOS => rewardedVideoIOS,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string GetRewardedInterstitialId()
        {
            return adPlatformType switch
            {
                EAdPlatformType.Editor => EDITOR_ID,
                EAdPlatformType.AndroidTest => rewardedInterstitialAndroidTest,
                EAdPlatformType.Android => rewardedInterstitialAndroid,
                EAdPlatformType.IOSTest => rewardedInterstitialIOSTest,
                EAdPlatformType.IOS => rewardedInterstitialIOS,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}