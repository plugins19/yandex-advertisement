namespace Runtime.Settings
{
    public interface IAdSettingsDatabase
    {
        float PreloadingInterval { get; }
        
        int LoadingAttempts { get; }
        
        string GetBannerId();

        string GetAppOpenId();

        string GetInterstitialId();

        string GetRewardedVideoID();

        string GetRewardedInterstitialId();
    }
}