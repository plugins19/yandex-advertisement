using Runtime.AdWrappers;
using Runtime.AdWrappers.AppOpen.Impls;
using Runtime.AdWrappers.Banner.Impls;
using Runtime.AdWrappers.Banner.Strategies.Impls;
using Runtime.AdWrappers.InitializeStrategies.Impls;
using Runtime.AdWrappers.Interstitial.Impls;
using Runtime.AdWrappers.Interstitial.Strategies.Impls;
using Runtime.AdWrappers.Main.Impls;
using Runtime.AdWrappers.RewardedVideo.Impls;
using Runtime.AdWrappers.RewardedVideo.Strategies.Impls;
using Runtime.AdWrappers.RewardInterstitial.Impls;
using Runtime.AdWrappers.RewardInterstitial.Strategies.Impls;
using Runtime.Helper.YandexSDK;
using Runtime.Settings.Impls;
using UnityEngine;
using Zenject;

namespace Runtime.Installers
{
    public class AdInstaller : MonoInstaller
    {
        [SerializeField] private EAdSourceType adSourceType;
        [SerializeField] private AdMobSettingsDatabase adMobSettingsDatabase;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<BannerWrapper>().AsSingle();
            Container.BindInterfacesTo<InterstitialWrapper>().AsSingle();
            Container.BindInterfacesTo<RewardedVideoWrapper>().AsSingle();
            Container.BindInterfacesTo<RewardedInterstitialWrapper>().AsSingle();
            
            switch (adSourceType)
            {
                case EAdSourceType.Editor:
                    Container.BindInterfacesTo<SimpleAdInitializeManager>().AsSingle();
                    Container.BindInterfacesTo<EditorBannerStrategy>().AsSingle();
                    Container.BindInterfacesTo<EditorInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<EditorRewardedVideoStrategy>().AsSingle();
                    Container.BindInterfacesTo<EmptyRewardedInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<EmptyAdInitializeStrategy>().AsSingle();
                    break;
                case EAdSourceType.YandexGames:
                    Container.BindInterfacesTo<SimpleAdInitializeManager>().AsSingle();
                    Container.BindInterfacesTo<YandexGameBannerStrategy>().AsSingle();
                    Container.BindInterfacesTo<YandexGameInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<YandexGameRewardedVideoStrategy>().AsSingle();
                    Container.BindInterfacesTo<EmptyRewardedInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<EmptyAdInitializeStrategy>().AsSingle();
                    var yandexSDKHelper = Container.InstantiateComponentOnNewGameObject<YandexSDKHelper>();
                    Container.Bind<YandexSDKHelper>().FromInstance(yandexSDKHelper).AsSingle();
                    break;
                case EAdSourceType.AdMob:
                    Container.BindInterfacesTo<AdMobInitializeManager>().AsSingle();
                    Container.BindInterfacesTo<AdMobBannerStrategy>().AsSingle();
                    Container.BindInterfacesTo<AdMobInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<AdMobRewardedVideoStrategy>().AsSingle();
                    Container.BindInterfacesTo<AdMobRewardedInterstitialStrategy>().AsSingle();
                    Container.BindInterfacesTo<AdMobAppOpenManager>().AsSingle();
                    Container.BindInterfacesTo<AdMobAdInitializeStrategy>().AsSingle();
                    Container.BindInterfacesTo<AdMobSettingsDatabase>().FromInstance(adMobSettingsDatabase).AsSingle();
                    break;
            }
        }
    }
}