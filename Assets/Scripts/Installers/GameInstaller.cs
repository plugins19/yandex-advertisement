using Ui;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private ButtonView buttonView;

        public override void InstallBindings()
        {
            var canvasInstance = Container.InstantiatePrefabForComponent<Canvas>(canvas);
            var canvasTransform = canvasInstance.transform;
            
            Container.BindInterfacesAndSelfTo<ButtonView>().FromComponentInNewPrefab(buttonView)
                .UnderTransform(canvasTransform).AsSingle();
            Container.BindInterfacesAndSelfTo<ButtonController>().AsSingle();
        }
    }
}